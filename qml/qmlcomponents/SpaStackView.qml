import QtQuick 2.0
import SpaComponents 1.0

Item {
    id: root
    property var controller
    property string initialView
    property bool backEnabled
    property var stack: []
    anchors.fill: parent


    Item {
        id: container
        anchors.fill: parent
    }


    function push(viewPath) {
        Log.debug("Pre-Push on stack", "Stack length: ", stack.length);
        var comp = Qt.createComponent(Qt.resolvedUrl(viewPath)).createObject(container, {});
        if (stack.length > 0) {
            var previous = stack[stack.length - 1];
            previous.visible = false;
        }
        comp.parent = container;
        comp.visible = true;
        stack.push(comp);
        backEnabled = stack.length > 1;
        Log.debug("Post-Push on stack", "Stack length: ", stack.length);
    }

    function pop() {
        Log.debug("Pre-Pop on stack", "Stack length: ", stack.length);
        var component = stack.pop();
        component.visible = false;
        component.destroy();
        var current = stack[stack.length - 1];
        current.parent = container;
        current.visible = true;
        backEnabled = stack.length > 1;
        Log.debug("Post-Pop on stack", "Stack length: ", stack.length);
    }

    Connections {
        target: controller
        onPushViewOnStack: {
            push(viewPath);
        }
        onPopViewFromStack: {
            pop();
        }
    }

    Component.onCompleted: {
        push(initialView);
    }

}
