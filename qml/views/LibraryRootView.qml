import QtQuick 2.0
import SpaComponents 1.0
import AppControllerEnums 1.0
import "../qmlcomponents"

Item {
    id: root
    y: 64
    width: 768
    height: 848
    clip: false
    visible: Viewer.renderState === SpaApplicationViewer.RenderStatePageGroupView && appController.viewState === AppController.LibraryViewState;

    SpaStackView {
        id: stackView
        controller: libraryRootViewController
        initialView: libraryRootViewController.defaultView
    }

    Item {
        id: toolbar

        width: 768
        height: 84
        y: 764

        SpaToolbarButton {
            id: backButton
            x: 17
            y: -4
            height: 85
            width: 130
            //: Light 20 114 1
            //: Back button to return to previous page in page group view
            //% "Back"
            text: qsTrId("000_BACK")
            type: "left"
            icon: Theme.path + "texticons/back.png"
            visible: true
            enabled: stackView.backEnabled
            onClicked: libraryRootViewController.backButtonClicked()
        }

        SpaToolbarButton {
            id: closeButton
            x: 621
            y: -4
            height: 85
            width: 130
            //: Light 20 114 1
            //: Close button on page group view
            //% "Close"
            text: qsTrId("000_CLOSE")
            type: "right"
            icon: Theme.path + "texticons/close.png"
            visible: true
            enabled: true
            onClicked: libraryRootViewController.closeButtonClicked()

        }
    }


}
