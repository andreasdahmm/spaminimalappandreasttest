import QtQuick 2.0
import SpaComponents 1.0

Item {
    id: root
    anchors.centerIn: parent

    SpaLabel{
        id: detailsLabel
        anchors.centerIn: parent
        fontClass: "infoText"
        text: "Details View"
    }
}
