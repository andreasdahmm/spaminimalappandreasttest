import QtQuick 2.0
import SpaComponents 1.0
import SpaHmiEnums 1.0


SpaMaximizedView {
    id: root

    SpaHomescreenToolbarBase {
        id: toolbar

        anchors.top: root.top
        anchors.left: root.left
        anchors.bottom: root.bottom
        anchors.topMargin: HmiProvider.fetchProperty(
                               HmiEnums.SPAHOMESCREENTOOLBAR_TOPMARGIN)
        anchors.leftMargin: HmiProvider.fetchProperty(
                                HmiEnums.SPAHOMESCREENTOOLBAR_LEFTMARGIN)

        textButtonCount: 2
        type: "009"

        //: Light 24 200 1
        //: go to library
        //% "Library"
        buttonText1: qsTrId("100_LIBRARY")
        onButton1Clicked: maximizedViewController.libraryButtonClicked()
        //: Light 24 200 1
        //: go to settings
        //% "Settings"
        buttonText2: qsTrId("100_SETTINGS")
        onButton2Clicked: maximizedViewController.settingsButtonClicked()
    }

    Item {
        id: mainContent

        anchors.left: toolbar.right
        anchors.top: root.top
        anchors.bottom: root.bottom
        anchors.right: root.right
        anchors.topMargin: HmiProvider.fetchProperty(
                               HmiEnums.SPAHOMESCREENLIST_TOPMARGIN)
        anchors.bottomMargin: 5
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        SpaLabel {
            id: helloLabel
            anchors.centerIn: mainContent
            fontClass: "infoText"
            text: "Hello, world!"
        }
    }
}
