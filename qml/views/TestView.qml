import QtQuick 2.0
import SpaComponents 1.0

Item {
    id: root
    anchors.centerIn: parent
    SpaLabel {
        id: testLabel
        anchors.centerIn: parent
        fontClass: "infoText"
        text: "TestView"
    }
}
