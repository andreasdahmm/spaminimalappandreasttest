import QtQuick 2.0
import SpaComponents 1.0
import LibraryRootViewControllerEnums 1.0

SpaLibraryPage {
    id: libraryStack1TabsContainer

    //: Light 24 200 1
    //: title browse library view
    //% "Browse"
    title: qsTrId("100_BROWSE")


    Item {
        id: root
        width: parent.width
        height: parent.height

        SpaLabel {
            id: browseLabel
            fontClass: "infoText"
            anchors.centerIn: parent
            text: "Browse View"
        }
        SpaFlatButton {
            id: detailsButton
            anchors.top: browseLabel.bottom
            anchors.right: parent.horizontalCenter
            onClicked: libraryRootViewController.onPushViewOnStack(LibraryRootViewController.DetailsView)
            text: "Open Details"
        }
        SpaFlatButton {
            id: testButton
            anchors.top: browseLabel.bottom
            anchors.left: parent.horizontalCenter
            onClicked: libraryRootViewController.onPushViewOnStack(LibraryRootViewController.TestView)
            text: "Open Test"
        }
    }

}


