import QtQuick 2.0
import SpaComponents 1.0
import AppControllerEnums 1.0

import "views/"



SpaApplicationArea {

    id: appWindow

    MaximizedView {
        id: maximizedView
        visible: Viewer.renderState === SpaApplicationViewer.RenderStateExpanded && appController.viewState === AppController.MaximizedViewState
    }

    SettingsView {
        id: settingsView
        visible: Viewer.renderState === SpaApplicationViewer.RenderStatePageGroupView && appController.viewState === AppController.SettingsViewState
    }

    LibraryRootView {
        id: libraryRootView
        visible: Viewer.renderState === SpaApplicationViewer.RenderStatePageGroupView && appController.viewState === AppController.LibraryViewState
    }

    Component.onCompleted: {
        // Set tile text

        //: Light 24 450 1
        //: App main text
        //% "Minimal App"
        Viewer.mainText = qsTrId("100_APP_MAIN_TEXT")
    }
}
