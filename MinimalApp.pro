# 0:NAVIGATION, 1:MEDIA, 2:TELEPHONY 3:OTHER
IHU_APPLICATION_CATEGORY = 3

TEMPLATE = app

QT             += quick network

SOURCES        += main.cpp \
    controllers/AppController.cpp \
    controllers/MaximizedViewController.cpp \
    controllers/SettingsViewController.cpp \
    controllers/LibraryRootViewController.cpp \
    controllers/BrowseViewController.cpp \

HEADERS        += \
    controllers/AppController.h \
    controllers/MaximizedViewController.h \
    controllers/SettingsViewController.h \
    controllers/LibraryRootViewController.h \
    controllers/BrowseViewController.h \

include (../SpaApplicationFramework/src/SpaApplicationFramework.pri)

DEFINES += NO_APPLICATION_HTTPCACHE
DEFINES += EXCLUDE_PHONE
DEFINES += EXCLUDE_CONTACTS
DEFINES += EXCLUDE_AUDIOINPUT
DEFINES += EXCLUDE_XMLPARSER
DEFINES += EXCLUDE_SPEECH
DEFINES += EXCLUDE_SPATILEMAP



OTHER_FILES += \
    qml/main.qml \
    ManifestFile.ini \

hack_for_lupdate {
    SOURCES += $$OTHER_FILES
}

DEPLOY_FILES += tile_icon.png

DISTFILES += \
    qml/main.qml \
    qml/views/MaximizedView.qml \
    ManifestFile.ini \
    qml/views/SettingsView.qml \
    qml/views/LibraryRootView.qml \
    qml/views/BrowseView.qml \
    qml/views/DetailsView.qml \
    qml/qmlcomponents/SpaStackView.qml \
    qml/views/TestView.qml

qmlsources.source   = qml
qmlsources.target   = .
DEPLOYMENTFOLDERS   += qmlsources

applicationDeployment()
