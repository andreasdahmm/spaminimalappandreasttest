#include "SpaApplication.h"
#include "SpaApplicationViewer.h"
#include "Log.h"

#include <QQmlContext>
#include <QQmlEngine>
#include <QtQml>

#include "controllers/AppController.h"
#include "controllers/MaximizedViewController.h"
#include "controllers/SettingsViewController.h"
#include "controllers/LibraryRootViewController.h"
#include "controllers/BrowseViewController.h"

int main(int argc, char *argv[])
{
    SpaApplication app(argc, argv);
    SpaApplicationViewer *viewer = SpaApplicationViewer::instance();

    viewer->init();
    QStringList eulas;
    eulas.append("eula");
    viewer->setRequiredEulas(eulas);

    AppController* appController = new AppController(viewer);
    MaximizedViewController* maximizedViewController = new MaximizedViewController(appController);
    SettingsViewController* settingsViewController = new SettingsViewController(appController);
    LibraryRootViewController* libraryRootViewController = new LibraryRootViewController(appController);
    BrowseViewController* browseViewController = new BrowseViewController(libraryRootViewController);

    qmlRegisterType<AppController>("AppControllerEnums", 1, 0, "AppController");
    qmlRegisterType<LibraryRootViewController>("LibraryRootViewControllerEnums", 1, 0, "LibraryRootViewController");

    viewer->rootContext()->setContextProperty("appController", appController);
    viewer->rootContext()->setContextProperty("maximizedViewController", maximizedViewController);
    viewer->rootContext()->setContextProperty("settingsViewController", settingsViewController);
    viewer->rootContext()->setContextProperty("libraryRootViewController", libraryRootViewController);
    viewer->rootContext()->setContextProperty("browseViewController", browseViewController);

    viewer->setSource(QStringLiteral("qml/main.qml"));
    viewer->show();



    Log::debug(QStringLiteral("main.cpp"), QStringLiteral("main"), QStringLiteral("Before app.exec()"));
    int exit_code = app.exec();
    Log::debug(QStringLiteral("main.cpp"), QStringLiteral("main"), QStringLiteral("Finished app.exec()"));
    return exit_code;
}
