#ifndef SETTINGSVIEWCONTROLLER_H
#define SETTINGSVIEWCONTROLLER_H

#include <QObject>
#include "AppController.h"

class SettingsViewController : public QObject
{
    Q_OBJECT
public:
    SettingsViewController(AppController* a, QObject *p = 0);

    Q_INVOKABLE void closeButtonClicked();

signals:
    void setViewState(AppController::ViewStateEnum);


private:
    AppController* _appController;
};

#endif // SETTINGSVIEWCONTROLLER_H
