#include "MaximizedViewController.h"

MaximizedViewController::MaximizedViewController(AppController* a, QObject *p) :
    QObject(p),
    _appController(a)
{
    connect(this, &MaximizedViewController::setViewState, _appController, &AppController::setViewState);
}

void MaximizedViewController::settingsButtonClicked() {
    emit setViewState(AppController::SettingsViewState);
}

void MaximizedViewController::libraryButtonClicked() {
    emit setViewState(AppController::LibraryViewState);
}
