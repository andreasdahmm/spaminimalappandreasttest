#ifndef LIBRARYROOTVIEWCONTROLLER_H
#define LIBRARYROOTVIEWCONTROLLER_H

#include <QObject>
#include <QStack>
#include <QMap>
#include <QUrl>
#include "AppController.h"

class LibraryRootViewController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString defaultView READ defaultView CONSTANT)

    Q_ENUMS(LibraryStackViews)
public:
    enum LibraryStackViews {
        BrowseView,
        DetailsView,
        TestView
    };

    LibraryRootViewController(AppController* a, QObject *p = 0);
    LibraryRootViewController(QObject *p = 0);

    Q_INVOKABLE void backButtonClicked();
    Q_INVOKABLE void closeButtonClicked();
    QString defaultView();

signals:
    void setViewState(AppController::ViewStateEnum);
    void pushViewOnStack(const QString& viewPath);
    void popViewFromStack();
    void backButton();

public slots:
    void onPushViewOnStack(LibraryStackViews view);
    void onPopViewFromStack();
    void onSetViewState();
private:
    AppController* _appController;
    QMap<LibraryStackViews, QString> _viewPathMap;
};

#endif // LIBRARYROOTVIEWCONTROLLER_H
