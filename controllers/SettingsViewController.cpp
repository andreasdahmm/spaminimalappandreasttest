#include "SettingsViewController.h"

SettingsViewController::SettingsViewController(AppController *a, QObject *p) :
    QObject(p),
    _appController(a)
{
    connect(this, &SettingsViewController::setViewState, _appController, &AppController::setViewState);
}

void SettingsViewController::closeButtonClicked()
{
    emit setViewState(AppController::MaximizedViewState);
}

