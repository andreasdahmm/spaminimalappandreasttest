#ifndef BROWSEVIEWCONTROLLER_H
#define BROWSEVIEWCONTROLLER_H

#include <QObject>
#include "LibraryRootViewController.h"


class BrowseViewController : public QObject
{
    Q_OBJECT
public:
    BrowseViewController(LibraryRootViewController* l, QObject *p = 0);


signals:

private:
    LibraryRootViewController* _libraryRootViewController;
};

#endif // BROWSEVIEWCONTROLLER_H
