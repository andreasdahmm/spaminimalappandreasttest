#ifndef MAXIMIZEDVIEWCONTROLLER_H
#define MAXIMIZEDVIEWCONTROLLER_H

#include "AppController.h"

class MaximizedViewController : public QObject
{
    Q_OBJECT
public:
    MaximizedViewController(AppController* a, QObject *p = 0);

    Q_INVOKABLE void settingsButtonClicked();
    Q_INVOKABLE void libraryButtonClicked();

signals:
    void setViewState(AppController::ViewStateEnum state);

private:
    AppController* _appController;
};

#endif // MAXIMIZEDVIEWCONTROLLER_H
