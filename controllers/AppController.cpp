#include "AppController.h"


AppController::AppController(SpaApplicationViewer* v, QObject *p) :
    QObject(p),
    _viewer(v),
    _viewState(MaximizedViewState)
{}

AppController::ViewStateEnum AppController::viewState() const
{
    return _viewState;
}

void AppController::setViewState(ViewStateEnum state)
{
    if (_viewState != state)
    {
        switch (state) {
        case MaximizedViewState:
            _viewer->closeLibraryView();
            break;
        case LibraryViewState:
        case SettingsViewState:
        default:
            _viewer->openLibraryView();
            break;
        }
        _viewState = state;
        emit viewStateChanged();
    }
}

