#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include "SpaApplicationViewer.h"


class AppController : public QObject
{
    Q_OBJECT

    Q_ENUMS(ViewStateEnum)

    Q_PROPERTY(ViewStateEnum viewState READ viewState WRITE setViewState NOTIFY viewStateChanged)


public:
    AppController(SpaApplicationViewer* v = 0, QObject* p = 0);

    enum ViewStateEnum
    {
        MaximizedViewState,
        LibraryViewState,
        SettingsViewState
    };

    ViewStateEnum viewState() const;


signals:
    void viewStateChanged();

public slots:
    void setViewState(ViewStateEnum state);
private:
    SpaApplicationViewer* _viewer;
    ViewStateEnum _viewState;
};

#endif // APPCONTROLLER_H
