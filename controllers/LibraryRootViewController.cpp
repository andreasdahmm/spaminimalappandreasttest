#include "LibraryRootViewController.h"

#include "Log.h"
LibraryRootViewController::LibraryRootViewController(AppController *a, QObject *p) :
    QObject(p),
    _appController(a),
    _viewPathMap()
{
    _viewPathMap[BrowseView] = "../views/BrowseView.qml";
    _viewPathMap[DetailsView] = "../views/DetailsView.qml";
    _viewPathMap[TestView] = "../views/TestView.qml";

    connect(this, &LibraryRootViewController::setViewState, _appController, &AppController::setViewState);
    connect(this, &LibraryRootViewController::backButton, this, &LibraryRootViewController::popViewFromStack);
    connect(_appController, &AppController::viewStateChanged, this, &LibraryRootViewController::onSetViewState);
}

LibraryRootViewController::LibraryRootViewController(QObject *p) :
    QObject(p)
{}

void LibraryRootViewController::backButtonClicked()
{
    emit backButton();
}

void LibraryRootViewController::closeButtonClicked()
{
    emit setViewState(AppController::MaximizedViewState);
}

QString LibraryRootViewController::defaultView()
{
    return _viewPathMap[BrowseView];
}

void LibraryRootViewController::onPushViewOnStack(LibraryStackViews view)
{
    emit pushViewOnStack(_viewPathMap[view]);
}

void LibraryRootViewController::onPopViewFromStack()
{
    emit popViewFromStack();
}

void LibraryRootViewController::onSetViewState()
{
    /*
    if (_appController->viewState() == AppController::LibraryViewState)
    {
        onPushViewOnStack(BrowseView);
    }
    */
}
